/*
 * Filename: ActorNode.h
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: Helps create nodes for our graph.
 */
#ifndef ACTORNODE_H
#define ACTORNODE_H
#include <string>
#include <vector>
#include <iostream>
#include <climits>

using namespace std;
/*Helps create edges in our graph. We need this
 * for our Actornode class.
 */
class ActorEdge;

/*
 * Class Name: ActorNode
 * Class Description: Constructs new nodes for each actor
 * in our graph.
 */
class ActorNode 
{
  public:
    string currName = ""; //Actors name.
    string prevName = ""; //Previous actor on path.
	
    vector<ActorEdge> connEdges; //Connections of edges. 
    ActorNode* currParent = NULL; //Parent of node.
 
    unsigned int dist = INT_MAX;  //Initial distance of node.
    string totalActor; // Keeps track of all the actors.
	
    int size = 0; //Size of our graph.	
    string connectionFilm = ""; //Connection of movie for traversal.
 
    bool done = false; //Check to see if Dijkstra's algorithm is done.
	
    ActorNode(); //Node object.
    ActorNode(string & a): currName(a) //Constructor for the node. 
    {}
};
#endif
