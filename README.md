This is a graph implementation that pairs movies and actors that solves the "Six Degrees of Kevin Bacon" problem:
https://en.wikipedia.org/wiki/Six_Degrees_of_Kevin_Bacon

With actors being nodes and movies being edges, you can run two executables.

 Run make to compile to the code and execute the given Makefile.
 
 You make need to make pathfinder and make actorconnections as well.

To find the shortest distance between two actors, run:

> ./pathfinder movie_casts.tsv u test_pairs.tsv out_paths_unweighted.tsv

You can modify the .tsv's to your liking with the same exact format. Use either u or w for the third argument
to specify a weighted or unweighted graph.

You can also run actorconnections.

The actorconnections program should answer
the following query for every actor pair (X, Y) in the given list: "After which year did actors X and
Y become connected?

Run actorconnections with the following:

> ./actorconnections movie_casts.tsv test_pairs.tsv out_connections_bfs.tsv ufind
 
 The last argument can be bfs or ufind to run either a bfs or unionfind algorithm.
 
 The second to last argument is some output file you specify. The other two inputs
 are the same as above for actorconnections.
 
 
 Enjoy!
 
 