CC=g++
CXXFLAGS=-std=c++11
LDFLAGS=

ifeq ($(type),opt)
    CPPFLAGS += -O3
    LDFLAGS += -O3
else
    CPPFLAGS += -g
    LDFLAGS += -g
endif

all: pathfinder actorconnections

pathfinder: ActorGraph.o

actorconnections: ActorGraph.o Disjoint.o

ActorGraph.o: Movie.h ActorNode.h ActorEdge.h ActorGraph.h

Disjoint.o: Disjoint.h Movie.h ActorNode.h ActorEdge.h


clean:
	rm -f pathfinder actorconnections *.o core*

