/*
 * Author: Luke Tribble
 * File Name: actorconnections.h
 * Description: Helps find links to kevin bacon
 * in our movie graph.
 */
#ifndef ACTORCONNECTIONS_H
#define ACTORCONNECTIONS_H
#include <string>
#include <unordered_map>
#include "Disjoint.h"
using namespace std;

/*
 * Class Name: actorconnections
 * Description: Helps find links
 * to kevin bacon in our movie graph.
 */
class actorconnections 
{
  public:
    //Finds all the links to kevin bacon in our graph.
    void actorConnections(const char* c, const char* p,
    std::ofstream & is, char u) const;
};
#endif
