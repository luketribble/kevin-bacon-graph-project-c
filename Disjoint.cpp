/*
 * Filename: Disjoint.cpp
 * Author: Luke Tribble
 * Date: 12-7-17
 * This file helps create disjoint sets
 * for the actorconnections file.
 */
#include <string>
#include <queue>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "Disjoint.h"
#include "Movie.h"
#include "ActorNode.h"
#include "ActorEdge.h"
#define TWO 2
#define THREE 3

using namespace std;

/*
 * Function Name: loadFromFile
 * Description: Takes in a char pointer filename
 * to load from a file to create a graph from.
 */
bool Disjoint::loadFromFile(const char* in_filename) 
{
  string movieHash;//Our movie for the graph.
  unordered_map<string, Movie>::iterator IteratorM; //loop through movies.
  unordered_map<string, ActorNode>::iterator IteratorA; //loop through actors.
  Movie * mObject; //pointers to our movies and actors.
  ActorNode * aObject; 
  ifstream infile(in_filename); //Initialize the filestream.
  bool have_header = false;

  while (infile) //Keep reading lines until we reach the end of the file.
  {
    string s;
    if (!getline( infile, s )) break; //Get next line.
    if (!have_header) //Skip the header.
    {
     have_header = true;
     continue;
    }
    istringstream ss( s );
    vector <string> record;
    while (ss) 
    {
     string next; //Get next string before hitting tab.
     if (!getline( ss, next, '\t' )) break;
     record.push_back( next );
    }
    if (record.size() != THREE) //Check for three columns.
    {
     continue;
    }

    string actor_name(record[0]);
    string movie_title(record[1]);
    int movie_year = stoi(record[TWO]);
    movieHash = record[1]+record[TWO]; //hash our key.
    
    IteratorA = totalActor.find(actor_name);
    ActorNode node_actor(actor_name);
    //Insert an actor object if it doesnt exist.Point to it otherwise.
    if(IteratorA != totalActor.end()) 
    {
      aObject = &IteratorA->second;
    }
    else 
    {
      aObject = &(totalActor.insert(make_pair
      (actor_name, node_actor)).first->second);
    }
    //Do the same thing for our movies.
    IteratorM = totalMovies.find(movieHash);
    Movie ourMov(movie_title, movie_year);

    if(IteratorM != totalMovies.end()) 
    {
     mObject = &IteratorM->second;
     mObject->ourActors.push_back(aObject);
    }
    else 
    {
     mObject = &(totalMovies.insert
     (make_pair(movieHash, ourMov)).first->second);
     mObject->ourActors.push_back(aObject);
    }
 }

 unordered_map<string, Movie>::iterator IteratorY;
 IteratorY = totalMovies.begin();

 while(IteratorY != totalMovies.end()) 
 { //Keep storing movies until end is reached.
   movieYears.push((*IteratorY).second);
   ++IteratorY;
 }
 infile.close();
 return true;
}

/*
 * Function Name: unionSet
 * Takes in two actornodes and creates a new set
 * combining the both of them.
 */
void Disjoint::unionSet(ActorNode* A, ActorNode* B) 
{
  ActorNode *x = find(A); //Find the two actors.
  ActorNode *y = find(B);
 
  if(x != y) 
  {  //If first node is less or equal to second,
    if(x->size <= y->size) 
    {
      x->currParent = y;//set first as parent of second
      y->size += x-> size; //seconds size is it's size plus first
    }
    else 
    {
      y->currParent = x; //otherwise set second's parent as first
      x->size+=y->size; //first one's size is second size plus its own size.
    }
 }
 return;
}

/*
 * Function Name: createGraph
 * Takes in a start and end node. Creates a graph
 * fron the input.
 */
int Disjoint::createGraph(string start, string end) 
{
  string movieHash; //Movie to be hashed.
  int index; //Index of actor.
  int movieActors; //How many actors in a movie.
  int totalPops; //How many movies were popped.

  vector<Movie> moviePush; //Used to store movies to push.
  unordered_map<string, ActorNode>::iterator IteratorR; 
  //Used to iterate nodes.
  //check if there is an existing actor after initializing fields.
  if(totalActor.find(start) == totalActor.end()) 
  {
    return 9999;
  }

  if(totalActor.find(end) == totalActor.end()) 
  {
    return 9999;
  }

 while((find(&totalActor.find(start)->second)->currName !=
 find(&totalActor.find(end)->second)->currName) && (!movieYears.empty())) 
 { //Make unions of the movies as we pop.
   Movie currMovie = movieYears.top(); 
   movieHash = currMovie.currTitle+to_string(currMovie.currYear);
   movieYears.pop();
   moviePush.push_back(currMovie);
   startYear = currMovie.currYear;
   movieActors = currMovie.ourActors.size();
   index = 0;
  //check if we have more than one actor to combine
  while(movieActors > 1) 
  {
   unionSet(totalMovies.find(movieHash)->
   second.ourActors[index],
   totalMovies.find(movieHash)->second.ourActors[index+1]);
   //return year if some path is existing. 
   if((find(&totalActor.find(start)->second)->
   currName ==
   find(&totalActor.find(end)->second)->currName)) {
   IteratorR = totalActor.begin();
   while(IteratorR != totalActor.end()) 
    { //reset iterator for the actors.
     IteratorR->second.size = 0;
     IteratorR->second.currParent = NULL;
     ++IteratorR;     
    }
    for(totalPops = 0; totalPops < moviePush.size(); ++totalPops) 
    { //Push movies to the queue.
     movieYears.push(moviePush[totalPops]);
    }
    return startYear;
   }
   ++index;
   --movieActors;
  }
 }
 //Reset value of the actor.
 IteratorR = totalActor.begin();

 while(IteratorR != totalActor.end()) 
 {
   IteratorR->second.currParent = NULL;
   IteratorR->second.size = 0;
   ++IteratorR;
 }
 //Pop movies back into queue.
 while(totalPops < moviePush.size()) 
 {
   movieYears.push(moviePush[totalPops]);
   ++totalPops;
 }
 return 9999; //Return 9999 if there is no connection.
}

/*
 * Function Name: find
 * Description: Takes in an actornode to
 * find.
 */
ActorNode* Disjoint::find(ActorNode* node) 
{
  if(node->currParent != NULL) 
  { //Check null base case, then call find recursively.
    return node->currParent = find(node->currParent);
  }
  else 
  {
    return node;
  }
}

/*
 * Function Name: BreadthFirstSearch
 * Description: Takes in a start and end node. Runs
 * BFS algorithm on an input.
 */
bool Disjoint::BreadthFirstSearch(string start, string end) 
{
  queue<ActorNode*> visits; //Keep track of visited paths. 
  unordered_map<std::string, ActorNode>::iterator current 
  = totalActor.find(start); //Iterators to go through actors.
  vector<ActorEdge>::iterator e;

  int numReset = actorReset.size(); 
  int totalPops = 0;
 
  if(numReset !=0) 
  { //Reset the previous node if needed.
    while(totalPops < numReset) 
    {
      actorReset.front() -> connectionFilm = "";
      actorReset.front() -> dist = INT_MAX;
      actorReset.front() -> prevName = "";
      actorReset.pop();
      ++totalPops;
    }
  }
 //Push node onto graph.
 visits.push(&current->second);
 //Loop until empty.
 while(!visits.empty()) 
 {
   ActorNode * n = visits.front();
   visits.pop();
   e = n->connEdges.begin();
   while(e != n->connEdges.end()) 
   { //Iterate through edges.
     if((*e).edge_end->dist == INT_MAX) 
     { //If maximum distance...
       (*e).edge_end->connectionFilm = (*e).filmTitle +
       to_string((*e).filmYear);
       (*e).edge_end->dist = n -> dist + 1;
       (*e).edge_end->prevName = n -> currName;
       visits.push((*e).edge_end);
       actorReset.push((*e).edge_end); //Reset end edge.
     }
     if((*e).edge_end->currName == end) 
     { //If edge is found, return true.
       return true;
     }
   ++e; //Increment edge number.
  }
 }
 return false;
}

/*
 * Function name: startBFS
 * Function Description: Runs the bfs
 * algorithm starting from a movie to be
 * taken in.
 */
void Disjoint::startBFS(Movie movie) 
{
  string title; //film 
  int year;  //year
  int weight; //weight
  vector<ActorNode*> a; //all our actornodes. 
  title = movie.currTitle; //intialize fields.
  a = movie.ourActors;
  year = movie.currYear;
  //loop through actors
  for(int i = 0; i < a.size()-1; i++) 
  {
    for(int j = i+1; j < a.size(); j++) 
    { //create edge from start to end and vice versa.
      weight = (1+(2015-year));
      movie.ourActors[i]->
      connEdges.emplace_back(a[i], a[j], title, year,weight);
      movie.ourActors[j]->
      connEdges.emplace_back(a[j], a[i], title, year,weight);
    }
  }
 return;
}

/*
 * Function Name: connectBFS
 * Description: Takes in a node to start and end on.
 * Continues running BFS after startBFS.
 */
int Disjoint::connectBFS(string start, string end) 
{
  int totalPops; //total pops.
  int index; //movie index.
  int current = movieYears.top().currYear; //current year
 
  vector<Movie> moviePush; //movies popped
  unordered_map<string, ActorNode>::iterator IteratorR; 
  //reset iterator
  if((totalActor.find(start) == totalActor.end()) 
  || (totalActor.find(end) == totalActor.end())) 
  {
    IteratorR = totalActor.begin();
    while(IteratorR != totalActor.end()) 
    {
      IteratorR->second.connEdges.clear();
      ++IteratorR;
    }
  return 9999; //return if actors dont exist.
 }

 while (!movieYears.empty()) //if there are still movies...
 {
   Movie m = movieYears.top();
   if(m.currYear == current) 
   { //if movie is same year pop the years
     movieYears.pop();
     moviePush.push_back(m);
   }
   if(movieYears.empty() || (m.currYear!=current)) 
   { //make edges
     for(index = 0; index < moviePush.size(); ++index) 
     {
       startBFS(moviePush[index]);
     }
   if(BreadthFirstSearch(start,end)) 
   { //if pair is found return year
     totalPops = 0;
     while(totalPops < moviePush.size()) 
     {
       movieYears.push(moviePush[totalPops]);
       ++totalPops; //push onto queue
     }
    IteratorR = totalActor.begin();
    while(IteratorR != totalActor.end()) 
    {
      IteratorR->second.connEdges.clear();
      ++IteratorR;
    } //clear edges and return year
    return current; 
   } //set current to movie year
   current = m.currYear;
  }
 }

 for(totalPops = 0; totalPops < moviePush.size(); ++totalPops ) 
 {
   movieYears.push(moviePush[totalPops]);
 } //push back to priority queue and clear edges.

 IteratorR = totalActor.begin();

 while(IteratorR != totalActor.end()) 
 {
   IteratorR->second.connEdges.clear();
   ++IteratorR;
 }
 return 9999; //return 9999 if nothing is found.
}

