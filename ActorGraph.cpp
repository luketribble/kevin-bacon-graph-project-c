/*
 * Filename: ActorGraph.cpp
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: This helps create our graph
 * to traverse through.
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <iterator>
#include <queue>
#include "ActorGraph.h"
#include "ActorEdge.h"
#include "ActorNode.h"
#define TWO 2
#define THREE 3


using namespace std;

class CompareDist
{
  public:
    bool operator() ( pair<int, ActorNode*> n1, pair<int, ActorNode*> n2)
    {
      return n1.first>n2.first;
    }
};	

/*
 * Function Name: loadFromFile
 * Description: Reads in a file to create a graph from.
 * Takes in file to read and boolean to create weighted
 * or unweighted graph.
 */
bool ActorGraph::loadFromFile(const char* in_filename, bool use_weighted_edges) 
{
  
  string movieHash; //Hash for the movie.
  //Iterate through our movies and actors.
  unordered_map<string, Movie>::iterator IteratorM;
  unordered_map<string, ActorNode>::iterator IteratorA;
    
  Movie * mObject;
  //Point to our actornodes and movies.
  ActorNode * aObject;
 
  ifstream infile(in_filename); //Create a filestream.

  bool have_header = false;

  while (infile) //Read in files until the end of file.
  {
    string s;

    if (!getline( infile, s )) break;

    if (!have_header) // Get the next line and skip the header.
    {
      have_header = true;
      continue;
    }
    istringstream ss( s );

    vector <string> record;

    while (ss) 
    {
      string next;
      // Get the next string before hitting a tab character.
      if (!getline( ss, next, '\t' )) break;

      record.push_back( next );
    } //Make sure we have 3 columns for our file.
    if (record.size() != THREE) 
    {
      continue;
    }

    string actor_name(record[0]);
    string movie_title(record[1]);
    int movie_year = stoi(record[TWO]);

    movieHash = record[1]+record[TWO]; 

    IteratorA = totalActors.find(actor_name);

    ActorNode node_actor(actor_name);
    //Insert an actorobject if it doesnt exist, otherwise point to it.
    if(IteratorA != totalActors.end()) 
    {
      aObject = &IteratorA->second;
    }
    else 
    {
      aObject = &(totalActors.insert
     (make_pair(actor_name, node_actor)).first-> second);
    }

    IteratorM = totalMovies.find(movieHash);

    Movie ourMov(movie_title, movie_year);
    //Do the same thing for the other iterator.
    if(IteratorM != totalMovies.end()) 
    {
      mObject = &IteratorM->second;
      mObject->ourActors.push_back(aObject);
    }
    else 
    {
      mObject = &(totalMovies.insert
      (make_pair(movieHash, ourMov)).first->second);
      mObject->ourActors.push_back(aObject);
    }
   }

   if (!infile.eof()) 
   {
     cerr << "Failed to read " << in_filename << "!\n";
     return false;
   }
    //Check for errors and create the graph.
    createGraph(use_weighted_edges);
    infile.close();

    return true;
}

/*
 * Function Name: BreadthFirstSarch
 * Description: Find the shortest path in a graph using bfs algorithm.
 * Takes in a start and end node.
 */
bool ActorGraph::BreadthFirstSearch(string start, string end) 
{
  queue<ActorNode*> visits; //Keep track of paths we visit.
 
  unordered_map<std::string, ActorNode>::iterator current 
  = totalActors.find(start);
  //Use map and vector to iterate through actors.
  vector<ActorEdge>::iterator e;

  int numReset = actorReset.size();
  int totalPops = 0; //Keep track of nodes and pops for reset.

	
  if(numReset !=0) 
  {
    while(totalPops < numReset) 
    {
      actorReset.front() -> connectionFilm = "";
      actorReset.front() -> dist = INT_MAX;
      actorReset.front() -> prevName = "";
      actorReset.pop();
      ++totalPops;
    }
  }

  visits.push(&current->second);
 	
  while(!visits.empty()) 
  {
    ActorNode * n = visits.front();
    visits.pop();
    e = n->connEdges.begin();
    //Iterator through edges until queue is empty.
    while(e != n->connEdges.end()) 
    {
      if((*e).edge_end->dist == INT_MAX) 
      {
        (*e).edge_end->connectionFilm = (*e).filmTitle + 
        to_string((*e).filmYear);
	(*e).edge_end->dist = n -> dist + 1;
	(*e).edge_end->prevName = n -> currName;
	visits.push((*e).edge_end);
	actorReset.push((*e).edge_end);
      } //If distance is maxxed, push the end edge.
      if((*e).edge_end->currName == end) 
      { //If edge is found, return true. 
        return true;
      }
      ++e;
    }
   }
   return false;
}

/*
 * Function Name: createGraph
 * Description: Creates our graph from
 * edges and actors. Takes in bool for 
 * weighted and unweighted creation.
 */
void ActorGraph:: createGraph(bool u) 
{
  ActorEdge* startEdge; 
  ActorEdge* endEdge;
 
  vector<ActorNode*> a; 
  unordered_map<string, Movie>::iterator current = totalMovies.begin();
  //Created needed fields and iterators to loop through movies.
  string title;

  int weight;
  int year;
  //Loop through all the movies.
  while(current != totalMovies.end()) 
  {
    title = current->second.currTitle;
    a = current->second.ourActors;
    year = current->second.currYear;
    //Loop through actors and create edges.
    for(int i = 0; i < a.size()-1; i++)
    {
      for(int j = i+1; j < a.size(); j++) 
      {
        weight = (1+(2015-year));
	current->second.ourActors[i]->
	connEdges.emplace_back(a[i], a[j], title, year, weight);
	current->second.ourActors[j]->
	connEdges.emplace_back(a[j], a[i], title, year, weight);
      }
    }
    ++current; //Iterate to the next movie.
  }
  return;
}

/*
 * Function Name: DijkstraAlgorithm
 * Description: Takes in a start and end node. This 
 * runs Dijkstras algorithm on a weighted graph.
 */
bool ActorGraph::DijkstraAlgorithm(string start, string end) 
{
  priority_queue<pair<int, ActorNode*>, //Queue to store weighted paths.
  vector<pair<int, ActorNode*>>, CompareDist> visits;
  unordered_map<std::string, ActorNode>::iterator current;
  pair<int, ActorNode*> v; //Fields use to iterate through edges.
  vector<ActorEdge>::iterator e;
 
  int totalPops = 0; 
  int numReset = actorReset.size(); 
  unsigned int currW; 

  if(numReset != 0) 
  {
    while(totalPops < numReset) 
    {
      actorReset.front()->connectionFilm = "";
      actorReset.front()->prevName = "";
      actorReset.front()->done = false;
      actorReset.front()->dist = INT_MAX;
      actorReset.pop();
      ++totalPops;
    }
  }
  //Initialize nodes, and then keep visiting each one.
  current = totalActors.find(start);
  (&current->second)->done = false;
  (&current->second)->dist = 0;
  visits.push(make_pair(0, &current->second));

  while(!visits.empty()) 
  {
    v= visits.top();
    //If end edge is found, return true.
    if(v.second->currName == end) 
    {
      return true;
    }

    visits.pop();
    //Start running the algorithm.
    if(!v.second->done) 
    {
      v.second -> done = true;
      e = v.second->connEdges.begin();

     while(e != v.second->connEdges.end()) 
     {
       currW = (*e).weight + v.second->dist;
       //If we have found a new smaller weight, update it.
       if(currW < (*e).edge_end->dist) 
       { //Update the node.
         (*e).edge_end->dist = currW;
	 (*e).edge_end->connectionFilm = (*e).filmTitle + to_string(((*e).filmYear));
	 (*e).edge_end->prevName = v.second->currName;
	 visits.push(make_pair(currW, (*e).edge_end));
	 actorReset.push((*e).edge_end);
       }
       ++e;
      }
     }
   }
  return false;
}		
