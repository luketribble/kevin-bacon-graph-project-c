/*
 * Filename: ActorGraph.h
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: This helps create
 * our graph to traverse through.
 */
#ifndef ACTORGRAPH_H
#define ACTORGRAPH_H

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <queue>
#include "Movie.h"
#include "ActorNode.h"
#include "ActorEdge.h"

using namespace std;

/* This class helps create our graph to traverse through. */

class ActorGraph 
{
protected:
  
public:
    unordered_map<std::string, Movie> totalMovies; //Move for the graph. 
    unordered_map<std::string, ActorNode> totalActors;  //Actors for the graph. 
    queue<ActorNode*> actorReset; //Used to reset nodes in the graph.
 
    /* Takes in a file to load from and a boolean to create weighted or
     * unweighted graph. Loads in file to create a graph from. */ 
    bool loadFromFile(const char* in_filename, bool use_weighted_edges);

    /* Takes in a start node and end node. Runs the BFS algorithm on a graph.*/
    bool BreadthFirstSearch(string s, string e);

    /* Creates a graph after a file is loaded in. Takes in a weighted edge. */
    void createGraph(bool weightededge);

    /* Runs Dijkstras Algorithm on a graph. Takes in start node and end node. */
    bool DijkstraAlgorithm(string s, string e);
  
};

#endif 
