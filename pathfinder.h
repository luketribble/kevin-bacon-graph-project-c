/*
 * File Name: pathfinder.h
 * Author: Luke Tribble
 * Description: Helps find the shortest path in a graph.
 */
#ifndef PATHFINDER_H
#define PATHFINDER_H
#include <string>
#include <unordered_map>
#include "ActorGraph.h"
using namespace std;

/*
 * Class Name: pathfinder
 * Desecription: Helps find the shortest path
 * in a graph.
 */
class pathfinder 
{
  public:
    bool pathFinder(const char* m, char u, const char* f,
    const char* output, std::ofstream & is) const; //helps output path.
};

#endif
