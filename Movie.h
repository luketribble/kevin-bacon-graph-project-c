/*
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: This allows for movie
 * objects to be created.
 */
#ifndef MOVIE_H
#define MOVIE_H
#include <string>
#include <vector>
#include <iostream>

using namespace std;
//Import the ActorNode class.
class ActorNode;

/*
 * Class Name: Movie
 * Description: Creates movie
 * objects for our graphs.
 */
class Movie 
{
  public:
    string currTitle; //title of movie
    vector<ActorNode*> ourActors;//corresponding actors
    int currYear; //year of movie			
    Movie(string & t, int & y): currTitle(t), currYear(y)
    {} //Movie constructor.
};
#endif
