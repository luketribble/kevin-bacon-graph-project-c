/*
 * Filename: Disjoint.h
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: Helps create disjoint sets.
 */


#ifndef DISJOINT_H
#define DISJOINT_H

#include <string>
#include <vector>
#include <unordered_map>
#include <queue>
#include <iostream>
#include "Movie.h"
#include "ActorNode.h"
#include "ActorEdge.h"

using namespace std;
/*
 * Classname: CompareYear
 * Description: Overload operator
 * to compare movie years and sort.
 */
class CompareYear
{
  public:
    bool operator() ( Movie one, Movie two) 
    {
      if(one.currYear == two.currYear) 
      {
        return true;
      }
      return one.currYear > two.currYear;
    }
};

/*
 * Classname: Disjoint
 * Description: Helps create disjoint sets.
 */
class Disjoint 
{
  protected:
  public:
    int startYear = 9999;//starting year

    //priority queue holds our movies and years.
    priority_queue<Movie, vector<Movie>, CompareYear> movieYears;
    queue<ActorNode*> actorReset; //used to reset nodes.
    unordered_map<string, Movie> totalMovies;//store total movies.
    unordered_map<string, ActorNode> totalActor;//store sum actors.
    //Creates a graph from a start and end node.
    int createGraph(string s, string e);
    //loads in a file to create a graph from.
    bool loadFromFile(const char* in_filename);
    //Finds an actornode given an input node.
    ActorNode* find(ActorNode* node);
    //Creates a new set containing nodes A and B.
    void unionSet(ActorNode* A, ActorNode* B); 
    //Performs BFS from a start node and ends on an end node.
    bool BreadthFirstSearch(string s, string e);
    //Starts performing BFS on a movie input.
    void startBFS(Movie movie);
    //Continues BFS on an start node and end node.
    int connectBFS(string s, string e);

  private:
};

#endif
