/*
 * Filename: actorconnections
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: Connects actors through years
 * using unionset or bfs.
 */
#include <vector>
#include <ostream>
#include <istream>
#include <fstream>
#include <iterator>
#include <queue>
#include <sstream>
#include <climits>
#include "actorconnections.h"
#include "ActorNode.h"
#include "ActorGraph.h"
#include "ActorEdge.h"
#include "Disjoint.h"
#include <time.h>
#define TWO 2
#define FOUR 4
#define THREE 3
using namespace std;

/*
 * Function Name: actorConnections
 * Takes in two datasets, a file to write out to
 * and a flag to determine if bfs or union is used.
 */
void actorConnections(const char *m, const char* f,
std::ofstream & is, char u) 
{
  int connectPair; //connect year pairs
  Disjoint g; //graph to use
  bool b = false; //use bfs or union?
  bool have_header = false; //jeader 
	
 if (u == 'b') 
 {
   b = true;
 }
 else 
 {
   b = false;
 }
//Use the same logic from the Actorgraph file:
 if(g.loadFromFile(m)) 
 {
   ifstream infile(f);
   while ((infile)) 
   {
     string s;
     if(!getline(infile, s)) break;
			
     if(!have_header) 
     {
       have_header = true;
       continue;
     }
			
     istringstream ss(s);
     vector<string> record;
			
     while(ss) 
     {
       string next;
       if(!getline(ss, next, '\t')) break;
       record.push_back(next);
     }
     if(record.size() != TWO) { continue; }
     string actor_start(record[0]);
     string actor_end(record[1]);
     //Build either union or BFS			
     if(b) 
     {
       connectPair = g.connectBFS(actor_start, actor_end);
     }
	
     else 
     {
       connectPair = g.createGraph(actor_start, actor_end);
     }
     //Take in the stream to the file			
     is << actor_start;
     is << "\t";
     is << actor_end;
     is << "\t";
     is << connectPair;
     is << "\n";
   }
  }
  else {}
}

/*
 * Function Name: Main
 * Description: Executes the actorconnections logic.
 */
int main(int argc, char* argv[]) 
{
  char flag_passing;
  if( argc < FOUR) //Check for arg and flag inputs
  {
    return 1;
  }
  if(argc == FOUR) 
  {
    flag_passing = 'u';
  }
  else 
  {
    string flag(argv[4]);
    if (flag == "bfs") 
    {
      flag_passing = 'b';
    }
    else 
    {
      flag_passing = 'u';
    }
  }

  ifstream input_one(argv[1]);
  ifstream input_two(argv[TWO]);
  //check to open files
  if (!input_one.is_open())
  {
    cout << "Invalid file" << endl;
    return -1;
  }
  if (!input_two.is_open())
  {
    cout << "Invalid file." << endl;
    return -1;
  }

  ofstream output(argv[THREE]);
  output.close();

  input_one.close();
  input_two.close();
  input_one.open(argv[1]);
  input_two.open(argv[TWO]);

  output.open(argv[THREE]);
  output << "Actor1\tActor2\tYear\n";
  //Time algoirthm and call main logic
  clock_t begin = clock();
  actorConnections(argv[1], argv[TWO], output, flag_passing);
  clock_t end = clock();
  double elapsed = double(end - begin) / CLOCKS_PER_SEC;
  input_one.close();
  input_two.close();
  output.close();
  cout << elapsed << endl;
  return 0;
}
