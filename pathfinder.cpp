/*
 * Filename: pathfinder.cpp
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: Finds the shortest 
 * paths between actors.
 */
#include <vector>
#include <iterator>
#include <ostream>
#include <fstream>
#include <istream>
#include <sstream>
#include <queue>
#include <climits>
#include "pathfinder.h"
#include "ActorGraph.h"
#include "ActorEdge.h"
#include "ActorNode.h"
#define TWO 2
#define THREE 3
#define FOUR 4
#define FIVE 5

using namespace std;
/*
 * Function Name: pathFinder
 * Description: Takes in two datasets to read from,
 * a file to write to, and a flag for weighted/unweighted.
 */
bool pathFinder(const char* m, char u, const char* f,
const char* output, std::ofstream & is) 
{
  string name;
  string yearTitle;
  string prevName; 
  string printName; 
  string title; 
  string film; 

  int year;
  int i; 
  int numElements; 
//Initilize graph and movie and actor fields.
  ActorGraph g; 
  bool un = false; 
  bool have_header = false;
 
  if (u == 'u') 
  {
    un = true;
  }
  else 
  {
    un = false;
  }
  if(g.loadFromFile(m, un)) //Follow starter code and load from file.
  {
    ifstream infile(f);
    while((infile)) 
    {
      string s;
      if(!getline(infile,s)) break;
      if(!have_header) 
      {
        have_header = true;
	continue;
      }
      istringstream ss(s);
      vector<string> record;
			
      while(ss) 
      {
        string next;
	if(!getline(ss, next, '\t')) break;
	record.push_back(next);
      }
      if(record.size() != TWO) 
      { 
        continue;
      }
      string actor_start(record[0]);
      string actor_end(record[1]);

      if(un) //Call BFS or Dijkstras algorithm.
      {
        g.BreadthFirstSearch(actor_start, actor_end);
      }
      else 
      {
        g.DijkstraAlgorithm(actor_start, actor_end);
      } //Set up vectors.
      name = g.totalActors.find(actor_end)->second.prevName;
      prevName = actor_end;
      vector<string> p;
      printName = "(" + prevName + ")";
      p.push_back(printName); //Create connections to print.
      film = g.totalActors.find(prevName)->second.connectionFilm;
      name = g.totalActors.find(prevName)->second.prevName;
      title = g.totalMovies.find(film)->second.currTitle;
      year = g.totalMovies.find(film)->second.currYear;
      yearTitle = "--[" +title+ "#@"+to_string(year) + "]-->";
      p.push_back(yearTitle);
      printName = "(" + name + ")";
      p.push_back(printName);
      prevName = name;	
      while(name != actor_start) //Search until we find our actor,
      {
        film = g.totalActors.find(prevName)->
	second.connectionFilm;
	name = g.totalActors.find(prevName)->
        second.prevName;
	title = g.totalMovies.find(film)->
	second.currTitle;
	year = g.totalMovies.find(film)->
	second.currYear;
        yearTitle = "--[" +title+ "#@"+to_string(year) + "]-->";
	p.push_back(yearTitle);
	printName = "(" + name + ")";
	p.push_back(printName);
	prevName = name;
      }
      numElements = p.size(); //Write all connections to output file.
      for(i = 0; i < numElements; ++i) 
      {
        is << p[numElements - i -1];
      }
      is.put('\n');
     }
    }
    else 
    {
      return false;
    }
}
	
/*
 * Function Name: main
 * Description: Executes the pathfinder code.
 */
int main(int argc, char* argv[]) 
{
  string flag; 
  char flag_passing; 
  if( argc != FIVE) 
  {
    return 1;
  }
  ifstream input_one(argv[1]);
  ifstream input_two(argv[THREE]); //Read in files.

  if (!input_one.is_open()) //Check if files don't exist.
  {
    cout << "Invalid file" << endl;
    return -1;
  }
  if (!input_two.is_open())
  {
    cout << "Invalid file." << endl;
    return -1;
  }
  flag = argv[TWO];
	
  if(flag == "u") //Check flags.
  {
    flag_passing = 'u';
  }
  else if(flag == "w") 
  {
    flag_passing = 'w';
  }
  else 
  {
   cout << "Wrong parameter '" << flag <<"', must be u or w" << endl;
   return 1;
  }
  ofstream output(argv[FOUR]); 
  output.close();	 
  input_one.close();
  input_two.close();
  input_one.open(argv[1]);
  input_two.open(argv[THREE]);
  output.open(argv[FOUR]); //Manage outputs and write to file.
  output << "(actor)--[movie#@year]-->(actor)--...\n";
  pathFinder(argv[1], *argv[TWO], argv[THREE], argv[FOUR], output);
  input_one.close();
  input_two.close();
  output.close();
  return 0;
}

	

