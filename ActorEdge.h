/*
 * Filename: ActorEdge.h
 * Author: Luke Tribble
 * Date: 12-7-17
 * Description: Helps create
 * a new edge in the graph.
 */
#ifndef ACTOREDGE_H
#define ACTOREDGE_H
#include <string>
#include <vector>
#include <iostream>

using namespace std;
//Import ActorNode Class
class ActorNode;

/*
 * Classname: ActorEdge
 * Description: Used to create
 * new edges in our graph.
 */
class ActorEdge 
{
  public:
    string filmTitle; //Name of the movie.
    int filmYear; //Year of the movie.

    ActorNode* edge_start; //Create an edge.
    ActorNode* edge_end; //Mark the end of an edge.

    int weight = 9999;
    //Construct a wefighted edge.
    ActorEdge(ActorNode* x, ActorNode* y, string title, int date, int w):
    filmTitle(title), filmYear(date), weight(w) 
    {
      edge_start = x;
      edge_end = y;
    }
    //Construct an unweighted edge.
    ActorEdge(ActorNode* &x, ActorNode* &y, string title, int year): 
    filmTitle(title), filmYear(year) 
    {
      edge_start = x;
      edge_end = y;
    }
};
#endif
	
		


